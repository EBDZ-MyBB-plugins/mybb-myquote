<?php

if(!defined("IN_MYBB"))
{
	die();
}

$plugins->add_hook("parse_message_htmlsanitized", "myquote_run");

function myquote_info() {
	return array(
		'name'          => 'Use own quote Tag',
		'description'   => '3 way of quote, without any title, with simple title and title with all data.',
		'compatibility' => '18*',
		'author'        => 'Betty',
		'authorsite'    => '',
		'version'       => '0.1.0',
		'codename'      => 'myquote',
	);
}



function myquote_run($message)
{
	$message = preg_replace(
		"#\[quote\](.*?)\[\/quote\](\r\n?|\n?)#si",
		"<blockquote class=\"mycode_quote\">$1</blockquote>\n",
		$message
	);
	$message = preg_replace(
		"#\[quote=\"(.*?)\"\](.*?)\[\/quote\](\r\n?|\n?)#si",
		"<blockquote class=\"mycode_quote\"><cite>$1</cite>$2</blockquote>\n",
		$message
	);
	return $message;
}
